Inspired by https://github.com/snipcart/directus-metalsmith-snipcart

To build,
```
nvm use --delete-prefix 6.9.1
matchsmith
```

To debug modules like matalsmith-directus:
```
DEBUG=metalsmith-directus metalsmith
```

Note: The metalsmith-directus plugin was broken and I fixed it in my fork.

Tip: To inspect objects in handlebars, us {{ log <object> }}